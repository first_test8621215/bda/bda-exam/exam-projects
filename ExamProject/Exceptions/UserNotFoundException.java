package ExamProject.Exceptions;

public class UserNotFoundException extends Exception{
    public UserNotFoundException(){
        super("ID is not in the list!");
    }
}
