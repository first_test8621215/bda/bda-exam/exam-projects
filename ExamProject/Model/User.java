package ExamProject.Model;

import java.util.HashMap;

public class User {
    public static HashMap<Integer, HashMap<String, String>> users = new HashMap<>();
    HashMap<String, String> userDetails = new HashMap<>();
    int personId;
    String username;
    String password;

    public User(int personId, String username, String password) {

            this.personId = personId;
            this.username = username;
            this.password = password;

            userDetails.put("Username",this.username);
            userDetails.put("Password",this.password);

            users.put(personId,userDetails);
    }
}
