package ExamProject.Model;

import java.util.HashMap;

public class Person {
    public static HashMap<Integer, HashMap<String, String>> people = new HashMap<>();
    HashMap<String, String> personDetails = new HashMap<>();
    public static int totalId = 1;
    public int id = 1;
    String name;
    String surname;
    String fatherName;
    int age;
    String gender;

    public Person(String name, String surname, String fatherName, int age, Gender gender) {
        this.id = totalId;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.age = age;
        this.gender = String.valueOf(gender);

        personDetails.put("Name", this.name);
        personDetails.put("Surname", this.surname);
        personDetails.put("Father Name", this.fatherName);
        personDetails.put("age", String.valueOf(this.age));
        personDetails.put("Gender", this.gender);

        people.put(this.id, personDetails);

        totalId += 1;
    }
}
