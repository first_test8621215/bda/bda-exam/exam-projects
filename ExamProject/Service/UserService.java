package ExamProject.Service;

import ExamProject.Exceptions.UserNotFoundException;

public interface UserService {
    public String registerUser() throws UserNotFoundException;
    public String showUsers();

}
