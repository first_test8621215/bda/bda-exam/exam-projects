package ExamProject.Service;

import ExamProject.Exceptions.UserNotFoundException;
import ExamProject.Model.Person;
import ExamProject.Model.User;

import java.util.Scanner;

public class UserServiceImpl implements UserService {
    @Override
    public String registerUser() throws UserNotFoundException {
        Scanner userDetailsStringScan = new Scanner(System.in);
        Scanner userDetailsIntScan = new Scanner(System.in);
        System.out.print("Enter the id that you wanna add as user: ");
        int userId = userDetailsIntScan.nextInt();

        try {
            if (Person.people.containsKey(userId)) {
                System.out.print("Username: ");
                String usernameInput = userDetailsStringScan.nextLine();
                System.out.print("Password: ");
                String passwordInput = userDetailsStringScan.nextLine();

                User usr = new User(userId, usernameInput, passwordInput);
                return userId + " added as user ! ";
            } else {
                throw new UserNotFoundException();
            }
        } catch (UserNotFoundException msg) {
            System.out.println(msg.getMessage());
        }
        return "";
    }

    @Override
    public String showUsers() {
        return User.users.toString();
    }
}
