package ExamProject.Service;

import ExamProject.Model.Gender;
import ExamProject.Model.Person;

import java.util.Arrays;
import java.util.Scanner;

public class PersonServiceImpl implements PersonService {
    @Override
    public String savePerson() {
        Scanner personDetailsStringScan = new Scanner(System.in);
        Scanner personDetailsIntScan = new Scanner(System.in);
        Scanner personDetailsGenderScan = new Scanner(System.in);


        System.out.print("Name: ");
        String name = personDetailsStringScan.nextLine();
        System.out.print("Surname: ");
        String surname = personDetailsStringScan.nextLine();
        System.out.print("Father Name: ");
        String fatherName = personDetailsStringScan.nextLine();
        System.out.print("Age: ");
        int age = personDetailsIntScan.nextInt();
        System.out.println("Gender: (Choose one of genders below)");
        System.out.println(Arrays.toString(Gender.values()));

        Gender gender = null;

        while (gender == null) {
            try {
                gender = Gender.valueOf(personDetailsGenderScan.nextLine());
            } catch (IllegalArgumentException e) {
                System.out.println("Please choose a value that is in the list!");
            }
        }
        Person prsn = new Person(name, surname, fatherName, age, gender);
        return prsn.id + " id person is created!";
    }

    @Override
    public String showPeople() {
        return Person.people.toString();
    }

}
