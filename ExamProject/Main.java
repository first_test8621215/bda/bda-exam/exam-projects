package ExamProject;
import ExamProject.Exceptions.UserNotFoundException;
import ExamProject.Service.PersonService;
import ExamProject.Service.PersonServiceImpl;
import ExamProject.Service.UserService;
import ExamProject.Service.UserServiceImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws UserNotFoundException {
        PersonService personService = new PersonServiceImpl();
        UserService userService = new UserServiceImpl();


        while (true) {
            Scanner inputValueScan = new Scanner(System.in);
            System.out.print("Your command: ");
            String inputValue = inputValueScan.nextLine();
            if (inputValue.equalsIgnoreCase("sp")) {
                System.out.println(personService.savePerson());
            } else if (inputValue.equalsIgnoreCase("ru")) {
                System.out.println(userService.registerUser());
            } else if (inputValue.equalsIgnoreCase("su")) {
                System.out.println(userService.showUsers());
            } else if (inputValue.equalsIgnoreCase("shp")) {
                System.out.println(personService.showPeople());
            } else if (inputValue.equalsIgnoreCase("exit")) {
                System.exit(0);
            } else {
                System.out.println("Unknown Command !");
            }
        }

    }
}
