package UniversityManagementSystem.Service;
import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Subject;
import java.sql.*;
import java.util.ArrayList;

public class SubjectServiceImpl implements SubjectService {
    @Override
    public ArrayList<Subject> allSubjects(ResultWrapper resultWrapper) throws SQLException {
        Statement all = resultWrapper.getAll();

        String query = "SELECT * FROM Subjects";
        ResultSet allsubjects = all.executeQuery(query);

        ArrayList<Subject> subjects = new ArrayList<Subject>();
        while (allsubjects.next()) {
            int subjectId = allsubjects.getInt("Id");
            String subjectName = allsubjects.getString("Subject");

            Subject sbj = new Subject(subjectId, subjectName);
            subjects.add(sbj);
        }
        resultWrapper.getConnection().close();
        all.close();
        return subjects;
    }

    @Override
    public Subject getSubjectFromId(ResultWrapper resultWrapper, int Id) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "SELECT * FROM Subjects WHERE Id=?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, Id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int subjectId = resultSet.getInt("Id");
                    String subjectName = resultSet.getString("Subject");

                    connection.close();
                    resultWrapper.getAll().close();
                    return new Subject(subjectId, subjectName);
                }
            }
        } catch (SQLException e) {
            connection.close();
            resultWrapper.getAll().close();
            e.printStackTrace();
        }

        connection.close();
        resultWrapper.getAll().close();
        return null;
    }

    @Override
    public void addSubject(ResultWrapper resultWrapper, Subject sbj) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "INSERT INTO Subjects(Subject) " +
                "VALUES (?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, sbj.getSubjectName());

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Subject inserted successfully.");
            } else {
                System.out.println("Subject insertion failed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        connection.close();
        resultWrapper.getAll().close();
    }

    @Override
    public void deleteSubject(ResultWrapper resultWrapper, Subject sbj) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "DELETE FROM Subjects WHERE Subject = ?";


        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, sbj.getSubjectName());

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Subject deleted successfully.");
            } else {
                System.out.println("Subject deleted failed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        connection.close();
        resultWrapper.getAll().close();
    }
}
