package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Journal;

import java.sql.SQLException;
import java.util.ArrayList;

public interface JournalService {
    ArrayList<Journal> getAllMarks(ResultWrapper resultWrapper) throws SQLException;
    ArrayList<Journal> getMarksForTeacher(ResultWrapper resultWrapper, int teacherId) throws SQLException;
    void putMark(ResultWrapper resultWrapper, Journal jrl) throws SQLException;
    void deleteMark(ResultWrapper resultWrapper, int journalId) throws SQLException;
    void changeMark(ResultWrapper resultWrapper, int journalId, int newMark) throws SQLException;
}
