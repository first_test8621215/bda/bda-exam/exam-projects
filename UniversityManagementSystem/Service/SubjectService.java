package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Subject;

import java.sql.SQLException;
import java.util.ArrayList;

public interface SubjectService {
    public ArrayList<Subject> allSubjects(ResultWrapper resultWrapper) throws SQLException;
    public Subject getSubjectFromId(ResultWrapper resultWrapper, int Id) throws SQLException;
    public void addSubject(ResultWrapper resultWrapper, Subject sbj) throws SQLException;
    public void deleteSubject(ResultWrapper resultWrapper, Subject sbj) throws SQLException;
}
