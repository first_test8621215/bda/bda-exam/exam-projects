package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.DBConnection;
import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import static UniversityManagementSystem.Service.StudentServiceImpl.searchStudent;

public class UserServiceImpl implements UserService {
    TeacherServiceImpl teacherService = new TeacherServiceImpl();
    StudentServiceImpl studentService = new StudentServiceImpl();
    JournalServiceImpl journalService = new JournalServiceImpl();

    @Override
    public <T> T logIn(String username, String password) throws SQLException {
        ResultWrapper resultWrapper = DBConnection.openConnection();
        Connection connection = resultWrapper.getConnection();

        String query = "SELECT teacherId, studentId FROM users WHERE username=? AND password=?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                if (resultSet.getInt("teacherId") != 0) {
                    return (T) TeacherServiceImpl.getTeacher(resultWrapper, resultSet.getInt("teacherId"));
                } else if (resultSet.getInt("studentId") != 0) {
                    return (T) StudentServiceImpl.getStudent(resultWrapper, resultSet.getInt("studentId"));
                }else{
                    return (T) new Admin(username, password);
                }
            } else {
                System.out.println("Incorrect credentials!");
            }
            return null;
        }
    }

    @Override
    public boolean studentInterface(Student std) throws SQLException {
        int studentId = std.getStudentId();
        Scanner operationValueScan = new Scanner(System.in);

        System.out.print("\nChoose the operation\n1.List my subjects\n2.List my marks\n3.Log out\nYour option: ");

        int operationValue = 0;
        try {
            operationValue = operationValueScan.nextInt();
        }catch(InputMismatchException msg){
            System.out.println("Invalid input! Try again!");
        }

        String query = "SELECT `Subject Id` FROM Teacher_To_Subject WHERE `Group Id`=(SELECT `Group Id` FROM Student_To_Group WHERE `Student Id`=?)";
        String query2 = "SELECT * FROM Journal WHERE `Student Id`=?";
        String query3 = "SELECT `Subject` FROM Subjects WHERE `Id`=?";
        String query4 = "SELECT `Name`,`Surname` FROM Teacher WHERE `Id`=?";
        switch (operationValue) {
            case 1:
                ResultWrapper resultWrapper = DBConnection.openConnection();
                Connection connection = resultWrapper.getConnection();
                try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setInt(1, studentId);

                    ResultSet resultSet = preparedStatement.executeQuery();

                    while (resultSet.next()) {
                        int subjectId = resultSet.getInt("Subject Id");

                        PreparedStatement preparedStatement1 = connection.prepareStatement(query3);
                        preparedStatement1.setInt(1, subjectId);

                        ResultSet resultSet1 = preparedStatement1.executeQuery();

                        String subjectName = null;
                        if (resultSet1.next()){
                            subjectName = resultSet1.getString("Subject");
                        }

                        Subject sbj = new Subject(subjectId, subjectName);
                        System.out.println(sbj);
                    }
                }

                connection.close();
                resultWrapper.getAll().close();
                break;
            case 2:
                ResultWrapper resultWrapper2 = DBConnection.openConnection();
                Connection connection2 = resultWrapper2.getConnection();

                try (PreparedStatement preparedStatement2 = connection2.prepareStatement(query2)) {
                    preparedStatement2.setInt(1, studentId);

                    ResultSet resultSet2 = preparedStatement2.executeQuery();

                    System.out.print("Student: " + std.getName() + " " + std.getSurname());
                    while (resultSet2.next()) {
                        int journalId = resultSet2.getInt("Id");
                        int teacherId = resultSet2.getInt("Teacher Id");
                        int groupId = resultSet2.getInt("Group Id");
                        int subjectId = resultSet2.getInt("Subject Id");
                        int mark = resultSet2.getInt("Mark");
                        Date date = resultSet2.getDate("Date");

                        Journal jrl = new Journal(journalId,teacherId,groupId,subjectId,mark,date);

                        PreparedStatement preparedStatement3 = connection2.prepareStatement(query3);
                        preparedStatement3.setInt(1,subjectId);
                        ResultSet resultSet3 = preparedStatement3.executeQuery();

                        String subjectName = null;
                        if(resultSet3.next()){
                            subjectName = resultSet3.getString("Subject");
                        }

                        PreparedStatement preparedStatement4 = connection2.prepareStatement(query4);
                        preparedStatement4.setInt(1,teacherId);
                        ResultSet resultSet4 = preparedStatement4.executeQuery();

                        String teacherName = null;
                        if(resultSet4.next()){
                            teacherName = resultSet4.getString("Name") + " " + resultSet4.getString("Surname");
                        }

                        System.out.print("\n\nSubject: " + subjectName +
                                "\nGroup Id: " + jrl.getGroupId() +
                                "\nTeacher: " + teacherName +
                                "\nMark: " + jrl.getMark() +
                                "\nDate: " + jrl.getDate() +
                                "\n");
                    }
                }

                connection2.close();
                resultWrapper2.getAll().close();
                break;
            case 3:
                return true;

        }
        return false;
    }

    @Override
    public boolean teacherInterface(Teacher teacher) throws SQLException {
        int teacherId = teacher.getTeacherId();

        System.out.print("\nChoose the operation\n1.List all teachers\n2.List all Students\n3.Search for specific student\n4.Put mark to a student\n5.Delete mark from a student\n6.Change student mark\n7.Log out\nYour option: ");
        Scanner operationValueScan = new Scanner(System.in);

        int operationValue = 0;
        try{
            operationValue = operationValueScan.nextInt();
        } catch (InputMismatchException msg){
            System.out.println("Invalid input! Try again!");
        }

        ArrayList<Journal> marks;
        int markId;

        String query;
        String query2;
        String query3;
        String query4;
        ResultWrapper resultWrapper;

        Scanner idScanner = new Scanner(System.in);
        Scanner markIdScanner = new Scanner(System.in);
        Scanner markScanner = new Scanner(System.in);
        Scanner dateScanner = new Scanner(System.in);

        switch (operationValue) {
            case 1:
                ResultWrapper resultWrapperTch = DBConnection.openConnection();
                ArrayList<Teacher> teachers = teacherService.getAllTeachers(resultWrapperTch);
                for (Teacher tch : teachers) {
                    System.out.println(tch.toString());
                }
                break;
            case 2:
                ResultWrapper resultWrapperStd = DBConnection.openConnection();
                ArrayList<Student> students = studentService.getAllStudents(resultWrapperStd);
                for (Student std : students) {
                    System.out.println(std.toString());
                }
                break;
            case 3:
                Scanner searchingValueScanStudent = new Scanner(System.in);
                System.out.print("For what parameter you would like to search?\n1.Name\n2.Surname\n3.Age\n4.Faculty\n5.Profession\n6.Course\n7.Exam Score\n8.Paid study or not\n9.Go back to previous menu\nYour option: ");
                int searchingValueStudent = searchingValueScanStudent.nextInt();
                searchStudent(searchingValueStudent);
                break;
            case 4:
                System.out.print("Enter Group Id: ");

                int groupId = idScanner.nextInt();

                resultWrapper = DBConnection.openConnection();
                Connection connection = resultWrapper.getConnection();

                query = "SELECT * FROM Student WHERE Id in (SELECT `Student Id` FROM Student_To_Group WHERE `Group Id`=?)";
                query2 = "SELECT * FROM Teacher_To_Subject WHERE `Teacher Id`=?";
                query3 = "SELECT * FROM Subjects where `Id`=?";
                query4 = "INSERT INTO Journal(`Student Id`,`Teacher Id`, `Group Id`, `Subject Id`, `Mark`, `Date`) VALUES (?,?,?,?,?,?)";
                try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setInt(1, groupId);

                    ResultSet resultSet = preparedStatement.executeQuery();

                    while (resultSet.next()) {
                        Student std = new Student(resultSet.getInt("Id"),
                                resultSet.getString("Name"),
                                resultSet.getString("Surname"),
                                resultSet.getInt("Age"),
                                resultSet.getString("Faculty"),
                                resultSet.getString("Profession"),
                                resultSet.getInt("Course"),
                                resultSet.getInt("ExamScore"),
                                resultSet.getBoolean("ifPaidStudy"),
                                groupId);

                        System.out.println(std);
                    }

                    System.out.print("Enter Student Id: ");
                    int studentId = idScanner.nextInt();

                    System.out.println("Choose a subject:  ");
                    PreparedStatement preparedStatement2 = connection.prepareStatement(query2);
                    preparedStatement2.setInt(1, teacherId);

                    ResultSet resultSet2 = preparedStatement2.executeQuery();
                    while (resultSet2.next()) {
                        PreparedStatement preparedStatement3 = connection.prepareStatement(query3);
                        preparedStatement3.setInt(1, resultSet2.getInt("Subject Id"));
                        ResultSet resultSet3 = preparedStatement3.executeQuery();
                        if (resultSet3.next()) {
                            System.out.println(resultSet3.getInt("Id") + ". " + resultSet3.getString("Subject"));
                        }
                    }

                    System.out.print("Enter Subject Id: ");
                    int subjectId = idScanner.nextInt();
                    System.out.print("Add Mark: ");
                    int mark = markScanner.nextInt();
                    System.out.println("Date (yyyy-MM-dd): ");
                    String date = dateScanner.nextLine();

                    try (PreparedStatement preparedStatement4 = connection.prepareStatement(query4, Statement.RETURN_GENERATED_KEYS)) {
                        Journal jrl = new Journal(studentId, teacherId, groupId, subjectId, mark, date);
                        preparedStatement4.setInt(1, jrl.getStudentId());
                        preparedStatement4.setInt(2, jrl.getTeacherId());
                        preparedStatement4.setInt(3, jrl.getGroupId());
                        preparedStatement4.setInt(4, jrl.getSubjectId());
                        preparedStatement4.setInt(5, jrl.getMark());

                        java.util.Date utilDate = jrl.getDate();
                        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                        preparedStatement4.setDate(6, sqlDate);

                        int rowsAffected = preparedStatement4.executeUpdate();
                        if (rowsAffected > 0) {
                            System.out.println("Mark added successfully!");
                            break;
                        }
                    }
                }
                break;
            case 5:
                ResultWrapper resultWrapper1 = DBConnection.openConnection();
                ResultWrapper resultWrapper2 = DBConnection.openConnection();

                marks = journalService.getMarksForTeacher(resultWrapper1, teacherId);

                for (Journal mark: marks){
                    System.out.println(mark);
                }

                System.out.print("Enter mark ID to delete: ");
                markId = markIdScanner.nextInt();

                journalService.deleteMark(resultWrapper2, markId);
                break;
            case 6:
                ResultWrapper resultWrapper3 = DBConnection.openConnection();
                ResultWrapper resultWrapper4 = DBConnection.openConnection();

                marks = journalService.getMarksForTeacher(resultWrapper3, teacherId);

                for (Journal mark: marks){
                    System.out.println(mark);
                }

                System.out.print("Enter mark ID to change: ");
                markId = markIdScanner.nextInt();
                System.out.print("Enter new mark: ");
                int mark = markScanner.nextInt();


                journalService.changeMark(resultWrapper4, markId, mark);
                break;
            case 7:
                return true;
        }
        return false;
    }


    @Override
    public boolean adminInterface(Admin admin) throws SQLException {
        System.out.print("\nChoose the operation:\n1.Add a student\n2.Add a teacher\n3.List all Students\n4.List all teachers\n5.Search for specific student\n6.Search for specific teacher\n7.Exit\nYour option: ");
        Scanner operationValueScan = new Scanner(System.in);
        int operationValue = operationValueScan.nextInt();
        switch (operationValue) {
            case 1:
                while (true) {
                    Scanner studentStringValuesScan = new Scanner(System.in);
                    Scanner studentIntegerValuesScan = new Scanner(System.in);
                    Scanner studentBooleanValuesScan = new Scanner(System.in);

                    String name = null;
                    String surname = null;
                    String faculty = null;
                    String profession = null;
                    int age = 0;
                    int course = 0;
                    int examScore = 0;
                    int groupId = 0;
                    Boolean ifPaidStudy = null;

                    try {
                        System.out.print("Name: ");
                        name = studentStringValuesScan.nextLine();
                        System.out.print("Surname: ");
                        surname = studentStringValuesScan.nextLine();
                        System.out.print("Age: ");
                        age = studentIntegerValuesScan.nextInt();
                        System.out.print("Faculty: ");
                        faculty = studentStringValuesScan.nextLine();
                        System.out.print("Profession: ");
                        profession = studentStringValuesScan.nextLine();
                        System.out.print("Course: ");
                        course = studentIntegerValuesScan.nextInt();
                        System.out.print("Exam Score: ");
                        examScore = studentIntegerValuesScan.nextInt();
                        System.out.print("If it is paid study? (true or false): ");
                        ifPaidStudy = studentBooleanValuesScan.nextBoolean();
                        System.out.print("Group ID: ");
                        groupId = studentIntegerValuesScan.nextInt();
                    } catch (InputMismatchException | NullPointerException msg) {
                        System.out.println(msg);
                        System.out.println("Student insertion failed");
                        continue;
                    }

                    Student std = new Student(name, surname, age, faculty, profession, course, examScore, ifPaidStudy, groupId);
                    ResultWrapper resultWrapper = DBConnection.openConnection();
                    new StudentServiceImpl().addStudent(resultWrapper, std);

                    System.out.println("Do you want to do another student? (Yes or No)");
                    String continuationValue = studentStringValuesScan.nextLine();
                    if (continuationValue.equalsIgnoreCase("yes") || continuationValue.equalsIgnoreCase("y")) {
                        continue;
                    } else if (continuationValue.equalsIgnoreCase("no") || continuationValue.equalsIgnoreCase("n")) {
                        break;
                    } else {
                        System.out.println("Unknown operand!");
                        break;
                    }
                }
                break;
            case 2:
                while (true) {
                    Scanner teacherStringValuesScan = new Scanner(System.in);
                    Scanner teacherIntegerValuesScan = new Scanner(System.in);

                    String name = null;
                    String surname = null;
                    int age = 0;
                    String academicDegree = null;
                    int salary = 0;

                    try {
                        System.out.print("Name: ");
                        name = teacherStringValuesScan.nextLine();
                        System.out.print("Surname: ");
                        surname = teacherStringValuesScan.nextLine();
                        System.out.print("Age: ");
                        age = teacherIntegerValuesScan.nextInt();
                        System.out.print("Academic Degree: ");
                        academicDegree = teacherStringValuesScan.nextLine();
                        System.out.print("Salary: ");
                        salary = teacherIntegerValuesScan.nextInt();
                    } catch(InputMismatchException msg){
                        System.out.println(msg);
                        System.out.println("Teacher insertion failed");
                        continue;
                    }

                    Teacher tch = new Teacher(name, surname, age, academicDegree, salary);
                    ResultWrapper resultWrapper = DBConnection.openConnection();
                    new TeacherServiceImpl().addTeacher(resultWrapper, tch);

                    System.out.println("Do you want to do another teacher? (Yes or No)");
                    String continuationValue = teacherStringValuesScan.nextLine();
                    if (continuationValue.equalsIgnoreCase("yes") || continuationValue.equalsIgnoreCase("y")) {
                        continue;
                    } else if (continuationValue.equalsIgnoreCase("no") || continuationValue.equalsIgnoreCase("n")) {
                        break;
                    } else {
                        System.out.println("Unknown operand!");
                        break;
                    }
                }
                break;
            case 3:
                ResultWrapper resultWrapperStd = DBConnection.openConnection();
                ArrayList<Student> students = studentService.getAllStudents(resultWrapperStd);
                for (Student std : students) {
                    System.out.println(std.toString());
                }
                break;
            case 4:
                ResultWrapper resultWrapperTch = DBConnection.openConnection();
                ArrayList<Teacher> teachers = teacherService.getAllTeachers(resultWrapperTch);
                for (Teacher tch : teachers) {
                    System.out.println(tch.toString());
                }
                break;
            case 5:
                Scanner searchingValueScanStudent = new Scanner(System.in);
                System.out.print("For what parameter you would like to search?\n1.Name\n2.Surname\n3.Age\n4.Faculty\n5.Profession\n6.Course\n7.Exam Score\n8.Paid study or not\n9.Go back to previous menu\nYour option: ");
                int searchingValueStudent = searchingValueScanStudent.nextInt();
                searchStudent(searchingValueStudent);
                break;

            case 6:
                Scanner searchingValueScanTeacher = new Scanner(System.in);
                System.out.print("For what parameter you would like to search?\n1.Name\n2.Surname\n3.Age\n4.Academic Degree\n5.Salary\n6.Profession\n7.Go back to previous menu\nYour option: ");
                int searchingValueTeacher = searchingValueScanTeacher.nextInt();
                TeacherServiceImpl.searchTeacher(searchingValueTeacher);
                break;

            case 7:
                return true;
        }
        return false;
    }
}
