package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.DBConnection;
import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Subject;
import UniversityManagementSystem.Model.Teacher;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;


public class TeacherServiceImpl implements TeacherService {
    public static void main(String[] args) throws SQLException {
        ResultWrapper resultWrapper = DBConnection.openConnection();

        ArrayList<Teacher> teachers = new TeacherServiceImpl().getAllTeachers(resultWrapper);

        ResultWrapper resultWrapper2 = DBConnection.openConnection();

        new TeacherServiceImpl().getTeacherSubjects(resultWrapper2,teachers.get(4));
    }

    public static void searchTeacher(int searchingValue) throws SQLException {
        String searchingValueString = "";
        boolean exit = false;

        while (!exit) {
            switch (searchingValue) {
                case 1 -> searchingValueString = "Name";
                case 2 -> searchingValueString = "Surname";
                case 3 -> searchingValueString = "Age";
                case 4 -> searchingValueString = "Academic Degree";
                case 5 -> searchingValueString = "Salary";
                case 6 -> searchingValueString = "Subject";
                case 7 -> exit = true;
                default -> System.out.println("Invalid input. Please try again.");
            }

            ResultWrapper resultWrapper = DBConnection.openConnection();
            ArrayList<Teacher> teachers = new TeacherServiceImpl().getAllTeachers(resultWrapper);

            if (!searchingValueString.isEmpty() & !searchingValueString.isBlank()) {
                Scanner keyValueScan = new Scanner(System.in);
                String keyValue;

                if (searchingValueString.equals("Age")) {
                    System.out.print("1.Exact Age\n2.Older than\n3.Younger than\nYour option: ");
                    Scanner olderYoungerScan = new Scanner(System.in);
                    int olderYounger = olderYoungerScan.nextInt();
                    System.out.print(searchingValueString + ": ");
                    keyValue = keyValueScan.nextLine();
                    for (Teacher tch : teachers) {
                        switch (olderYounger) {
                            case 1:
                                if (tch.getAge() == Integer.parseInt(keyValue)) {
                                    System.out.println(tch.toString());
                                }
                                break;
                            case 2:
                                if (tch.getAge() > Integer.parseInt(keyValue)) {
                                    System.out.println(tch.toString());
                                }
                                break;
                            case 3:
                                if (tch.getAge() < Integer.parseInt(keyValue)) {
                                    System.out.println(tch);
                                }
                                break;
                        }
                    }
                } else if (searchingValueString.equals("Salary")) {
                    System.out.print("1.Exact salary\n2.More than\n3.Less than\nYour option: ");
                    Scanner moreLessScan = new Scanner(System.in);
                    int moreLess = moreLessScan.nextInt();
                    System.out.print(searchingValueString + ": ");
                    keyValue = keyValueScan.nextLine().toLowerCase();
                    for (Teacher tch : teachers) {
                        switch (moreLess) {
                            case 1 -> {
                                if (tch.getSalary() == Integer.parseInt(keyValue)) {
                                    System.out.println(tch.toString());
                                }
                            }
                            case 2 -> {
                                if (tch.getSalary() > Integer.parseInt(keyValue)) {
                                    System.out.println(tch.toString());
                                }
                            }
                            case 3 -> {
                                if (tch.getSalary() < Integer.parseInt(keyValue)) {
                                    System.out.println(tch.toString());
                                }
                            }
                        }
                    }
                } else {
                    System.out.print(searchingValueString + ": ");
                    keyValue = keyValueScan.nextLine();
                    if (searchingValueString.equalsIgnoreCase("name")) {
                        for (Teacher tch : teachers) {
                            if (tch.getName().toLowerCase().equalsIgnoreCase(keyValue)) {
                                System.out.println(tch.toString());
                            }
                        }
                    } else if (searchingValueString.equalsIgnoreCase("surname")) {
                        for (Teacher tch : teachers) {
                            if (tch.getSurname().toLowerCase().equalsIgnoreCase(keyValue)) {
                                System.out.println(tch.toString());
                            }
                        }
                    } else if (searchingValueString.equalsIgnoreCase("academic degree")) {
                        for (Teacher tch : teachers) {
                            if (tch.getAcademicDegree().toLowerCase().equalsIgnoreCase(keyValue)) {
                                System.out.println(tch.toString());
                            }
                        }
                    } else {
                        System.out.println("Invalid searching value!");
                    }
                }
                exit = true;
            }
        }
    }

    public static Teacher getTeacher(ResultWrapper resultWrapper, int teacherId) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "SELECT Name, Surname, Age, AcademicDegree, Salary " +
                "FROM teacher WHERE Id = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1,teacherId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                Teacher teacher =  new Teacher(teacherId, resultSet.getString("Name"),resultSet.getString("Surname"),resultSet.getInt("Age"),resultSet.getString("AcademicDegree"),resultSet.getInt("Salary"));
                connection.close();
                resultWrapper.getAll().close();
                return teacher;
            }
        }
        connection.close();
        resultWrapper.getAll().close();
        return null;
    }

    @Override
    public ArrayList<Teacher> getAllTeachers(ResultWrapper resultWrapper) throws SQLException {
        Statement all = resultWrapper.getAll();

        ResultSet allTeachers = all.executeQuery("select * from teacher");
        ArrayList<Teacher> teachers = new ArrayList<>();

        while (allTeachers.next()) {
            int teacherId = allTeachers.getInt("Id");
            String name = allTeachers.getString("Name");
            String surname = allTeachers.getString("Surname");
            int age = allTeachers.getInt("Age");
            String academicDegree = allTeachers.getString("AcademicDegree");
            int salary = allTeachers.getInt("Salary");

            Teacher tch = new Teacher(teacherId, name, surname, age, academicDegree, salary);
            teachers.add(tch);
        }

        resultWrapper.getConnection().close();
        all.close();
        return teachers;
    }

    @Override
    public void addTeacher(ResultWrapper resultWrapper, Teacher tch) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "INSERT INTO teacher(Name, Surname, Age, AcademicDegree, Salary) " +
                "VALUES (?, ?, ?, ?, ?)";


        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, tch.getName());
            preparedStatement.setString(2, tch.getSurname());
            preparedStatement.setInt(3, tch.getAge());
            preparedStatement.setString(4, tch.getAcademicDegree());
            preparedStatement.setInt(5, tch.getSalary());

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Teacher inserted successfully.");
            } else {
                System.out.println("teacher insertion failed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        resultWrapper.getAll().close();
    }

    @Override
    public void getTeacherSubjects(ResultWrapper resultWrapper, Teacher tch) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "SELECT Id FROM Teacher WHERE Name=? AND Surname=? AND Age=? AND AcademicDegree=? AND Salary=?";
        String query2 = "SELECT `Subject Id` FROM Teacher_To_Subject WHERE `Teacher Id`=?";
        String query3 = "SELECT Subject from Subjects WHERE Id=?";

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, tch.getName());
            preparedStatement.setString(2, tch.getSurname());
            preparedStatement.setInt(3, tch.getAge());
            preparedStatement.setString(4, tch.getAcademicDegree());
            preparedStatement.setInt(5, tch.getSalary());

            ResultSet teacherIdResultSet = preparedStatement.executeQuery();

            if (teacherIdResultSet.next()) {
                int teacherId = teacherIdResultSet.getInt("Id");
                try (PreparedStatement preparedStatement2 = connection.prepareStatement(query2)) {
                    preparedStatement2.setInt(1, teacherId);

                    ResultSet SubjectIdResultSet = preparedStatement2.executeQuery();

                    while (SubjectIdResultSet.next()) {
                        int subjectId = SubjectIdResultSet.getInt("Subject Id");
                        try (PreparedStatement preparedStatement3 = connection.prepareStatement(query3)) {
                            preparedStatement3.setInt(1,subjectId);

                            ResultSet subjectNamesResultSet = preparedStatement3.executeQuery();

                            if (subjectNamesResultSet.next()){
                                Subject sbj = new Subject(subjectNamesResultSet.getString("Subject"));
                                System.out.println(sbj);
                            }
                        }
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Teacher not found.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
