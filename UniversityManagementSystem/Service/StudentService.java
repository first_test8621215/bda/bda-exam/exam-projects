package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Student;

import java.sql.SQLException;
import java.util.ArrayList;

public interface StudentService {
    static void searchStudent(int searchingValue) {}
    ArrayList<Student> getAllStudents(ResultWrapper resultWrapper) throws SQLException;
    void addStudent(ResultWrapper resultWrapper, Student std) throws SQLException;
    }
