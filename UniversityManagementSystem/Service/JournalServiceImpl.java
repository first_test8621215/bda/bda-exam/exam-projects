package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Journal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class JournalServiceImpl implements JournalService {
    @Override
    public ArrayList<Journal> getAllMarks(ResultWrapper resultWrapper) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "SELECT * FROM Journal";

        ArrayList<Journal> marks = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)){
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                int journalId = resultSet.getInt("Id");
                int studentId = resultSet.getInt("Student Id");
                int teacherId = resultSet.getInt("Teacher Id");
                int groupId = resultSet.getInt("Group Id");
                int subjectId = resultSet.getInt("Subject Id");
                int mark = resultSet.getInt("Mark");
                Date date = resultSet.getDate("Date");

                Journal jrl = new Journal(journalId, studentId, teacherId, groupId, subjectId, mark, date);
                marks.add(jrl);
            }
        }
        return marks;
    }

    @Override
    public ArrayList<Journal> getMarksForTeacher(ResultWrapper resultWrapper, int teacherId) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "SELECT * FROM Journal WHERE `Teacher Id`=?";
        ArrayList<Journal> marks = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setInt(1,teacherId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                int journalId = resultSet.getInt("Id");
                int studentId = resultSet.getInt("Student Id");
                int groupId = resultSet.getInt("Group Id");
                int subjectId = resultSet.getInt("Subject Id");
                int mark = resultSet.getInt("Mark");
                Date date = resultSet.getDate("Date");

                Journal jrl = new Journal(journalId, studentId, teacherId, groupId, subjectId, mark, date);
                marks.add(jrl);
            }
        }
        connection.close();
        resultWrapper.getAll().close();
        return marks;
    }


    @Override
    public void putMark(ResultWrapper resultWrapper, Journal jrl) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "INSERT INTO Journal(`Student Id`,`Teacher Id`, `Group Id`, `Subject Id`, `Mark`, `Date`) VALUES (?,?,?,?,?,?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, jrl.getStudentId());
            preparedStatement.setInt(2, jrl.getTeacherId());
            preparedStatement.setInt(3, jrl.getGroupId());
            preparedStatement.setInt(4, jrl.getSubjectId());
            preparedStatement.setInt(5, jrl.getMark());
            preparedStatement.setDate(6, new java.sql.Date(jrl.getDate().getTime()));

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Mark added successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        resultWrapper.getAll().close();
    }

    @Override
    public void deleteMark(ResultWrapper resultWrapper, int journalId) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "DELETE FROM Journal WHERE Id=?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, journalId);

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Mark deleted successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        resultWrapper.getAll().close();

    }

    @Override
    public void changeMark(ResultWrapper resultWrapper, int journalId, int newMark) throws SQLException {
        Connection connection = resultWrapper.getConnection();

        String query = "UPDATE Journal SET `Mark`=? WHERE Id=?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, newMark);
            preparedStatement.setInt(2, journalId);

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Mark changed successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        resultWrapper.getAll().close();
    }
}
