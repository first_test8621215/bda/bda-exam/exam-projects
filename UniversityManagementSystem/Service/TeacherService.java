package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Teacher;

import java.sql.SQLException;
import java.util.ArrayList;

public interface TeacherService {
     static void searchTeacher(int searchingValue) throws SQLException {}
     ArrayList<Teacher> getAllTeachers(ResultWrapper resultWrapper) throws SQLException;
     void addTeacher(ResultWrapper resultWrapper, Teacher tch) throws SQLException;
     void getTeacherSubjects(ResultWrapper resultWrapper, Teacher tch) throws SQLException;
}
