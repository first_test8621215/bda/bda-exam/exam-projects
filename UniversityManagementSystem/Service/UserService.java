package UniversityManagementSystem.Service;

import UniversityManagementSystem.Model.Admin;
import UniversityManagementSystem.Model.Student;
import UniversityManagementSystem.Model.Teacher;

import java.sql.SQLException;

public interface UserService {
    <T> T logIn(String username,String password) throws SQLException;
    boolean studentInterface(Student std) throws SQLException;
    boolean teacherInterface(Teacher tch) throws SQLException;
    boolean adminInterface(Admin admin) throws SQLException;

}
