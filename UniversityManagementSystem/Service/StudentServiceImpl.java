package UniversityManagementSystem.Service;

import UniversityManagementSystem.DBConfig.DBConnection;
import UniversityManagementSystem.DBConfig.ResultWrapper;
import UniversityManagementSystem.Model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class StudentServiceImpl implements StudentService {
    public static void searchStudent(int searchingValue) throws SQLException {
        ResultWrapper resultWrapper = DBConnection.openConnection();
        ArrayList<Student> students = new StudentServiceImpl().getAllStudents(resultWrapper);

        String searchingValueString = "";
        boolean exit = false;

        while (!exit) {
            switch (searchingValue) {
                case 1 -> searchingValueString = "Name";
                case 2 -> searchingValueString = "Surname";
                case 3 -> searchingValueString = "Age";
                case 4 -> searchingValueString = "Faculty";
                case 5 -> searchingValueString = "Profession";
                case 6 -> searchingValueString = "Course";
                case 7 -> searchingValueString = "Exam Score";
                case 8 -> searchingValueString = "Paid Study";
                case 9 -> exit = true;
                default -> System.out.println("Invalid input. Please try again.");
            }


            if (!searchingValueString.isEmpty() & !searchingValueString.isBlank()) {
                Scanner keyValueScan = new Scanner(System.in);

                String keyValue = null;
                System.out.print(searchingValueString + ": ");
                try {
                    keyValue = keyValueScan.nextLine().toLowerCase();
                } catch (InputMismatchException msg) {
                    System.out.println("Invalid input!");
                }

                if (searchingValueString.equalsIgnoreCase("age")) {
                    System.out.print("1.Exact Age\n2.Older than\n3.Younger than\nYour option: ");
                    Scanner olderYoungerScan = new Scanner(System.in);
                    int olderYounger = 0;
                    try {
                        olderYounger = olderYoungerScan.nextInt();
                    } catch (InputMismatchException msg) {
                        System.out.println(msg);
                        System.out.println("Invalid input. Please try again.");
                        continue;
                    }
                    for (Student std : students) {
                        switch (olderYounger) {
                            case 1:
                                if (std.getAge() == Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                            case 2:
                                if (std.getAge() > Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                            case 3:
                                if (std.getAge() < Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                        }
                    }
                } else if (searchingValueString.equalsIgnoreCase("exam score")) {
                    System.out.print("1.Exact Score\n2.Higher than\n3.Lower than\nYour option: ");
                    Scanner higherLowerScan = new Scanner(System.in);
                    int higherLower = 0;
                    try {
                        higherLower = higherLowerScan.nextInt();
                    } catch (InputMismatchException msg) {
                        System.out.println(msg);
                        System.out.println("Invalid input. Please try again.");
                    }
                    for (Student std : students) {
                        switch (higherLower) {
                            case 1:
                                if (std.getExamScore() == Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                            case 2:
                                if (std.getExamScore() > Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                            case 3:
                                if (std.getExamScore() < Integer.parseInt(keyValue)) {
                                    System.out.println(std);
                                }
                                break;
                        }
                    }
                } else if (searchingValueString.equalsIgnoreCase("course")) {
                    System.out.print("1.Exact Course\n2.Higher than\n3.Lower than\nYour option: ");
                    Scanner higherLowerScan = new Scanner(System.in);
                    int higherLower = 0;
                    try {
                        higherLower = higherLowerScan.nextInt();
                    } catch (InputMismatchException msg) {
                        System.out.println(msg);
                        System.out.println("Invalid input. Please try again.");
                        for (Student std : students) {
                            switch (higherLower) {
                                case 1:
                                    if (std.getCourse() == Integer.parseInt(keyValue)) {
                                        System.out.println(std);
                                    }
                                    break;
                                case 2:
                                    if (std.getCourse() > Integer.parseInt(keyValue)) {
                                        System.out.println(std);
                                    }
                                    break;
                                case 3:
                                    if (std.getCourse() < Integer.parseInt(keyValue)) {
                                        System.out.println(std);
                                    }
                                    break;
                            }
                        }
                    }
                }else{
                        if (searchingValueString.equalsIgnoreCase("name")) {
                            for (Student std : students) {
                                if (std.getName().toLowerCase().equals(keyValue)) {
                                    System.out.println(std);
                                }
                            }
                        } else if (searchingValueString.equalsIgnoreCase("surname")) {
                            for (Student std : students) {
                                if (std.getSurname().toLowerCase().equals(keyValue)) {
                                    System.out.println(std);
                                }
                            }
                        } else if (searchingValueString.equalsIgnoreCase("faculty")) {
                            for (Student std : students) {
                                if (std.getFaculty().toLowerCase().equals(keyValue)) {
                                    System.out.println(std);
                                }
                            }
                        } else if (searchingValueString.equalsIgnoreCase("profession")) {
                            for (Student std : students) {
                                if (std.getProfession().toLowerCase().equals(keyValue)) {
                                    System.out.println(std);
                                }
                            }
                        } else if (searchingValueString.equalsIgnoreCase("paid study")) {
                            for (Student std : students) {
                                if (String.valueOf(std.isIfPaidStudy()).equalsIgnoreCase(keyValue)) {
                                    System.out.println(std);
                                }
                            }
                        }
                    }
                }

                exit = true;
            }
        }


    public static Student getStudent(ResultWrapper resultWrapper, int studentId) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "SELECT Id,Name, Surname, Age, Faculty, Profession, Course, ExamScore, ifPaidStudy " +
                "FROM student WHERE Id = ?";
        String query2 = "SELECT `Group Id` from Student_To_Group WHERE `Student Id` = ?";

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement2 = null;
        ResultSet resultSet2 = null;

        try {
            preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, studentId);

            resultSet = preparedStatement.executeQuery();

            int groupId = 0;
            if (resultSet.next()) {
                preparedStatement2 = connection.prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
                preparedStatement2.setInt(1, studentId);

                resultSet2 = preparedStatement2.executeQuery();
                if (resultSet2.next()) {
                    groupId = resultSet2.getInt("Group Id");
                }
            }

            return new Student(resultSet.getInt("Id"),
                    resultSet.getString("Name"),
                    resultSet.getString("Surname"),
                    resultSet.getInt("Age"),
                    resultSet.getString("Faculty"),
                    resultSet.getString("Profession"),
                    resultSet.getInt("Course"),
                    resultSet.getInt("ExamScore"),
                    resultSet.getBoolean("ifPaidStudy"),
                    groupId);
        } finally {
            if (resultSet2 != null) {
                resultSet2.close();
            }
            if (preparedStatement2 != null) {
                preparedStatement2.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            connection.close();
            resultWrapper.getAll().close();
        }
    }


    public ArrayList<Student> getAllStudents(ResultWrapper resultWrapper) throws SQLException {
        Statement all = resultWrapper.getAll();
        ResultSet allStudents = all.executeQuery("SELECT * FROM student");
        ArrayList<Student> students = new ArrayList<>();

        while (allStudents.next()) {
            int studentId = allStudents.getInt("Id");
            String name = allStudents.getString("Name");
            String surname = allStudents.getString("Surname");
            int age = allStudents.getInt("Age");
            String faculty = allStudents.getString("Faculty");
            String profession = allStudents.getString("Profession");
            int course = allStudents.getInt("Course");
            int examScore = allStudents.getInt("ExamScore");
            boolean ifPaidStudy = allStudents.getBoolean("ifPaidStudy");
            int groupId = getGroupIdForStudent(resultWrapper, studentId);

            Student std = new Student(studentId, name, surname, age, faculty, profession, course, examScore, ifPaidStudy, groupId);
            students.add(std);
        }

        resultWrapper.getConnection().close();
        all.close();
        return students;
    }

    private int getGroupIdForStudent(ResultWrapper resultWrapper, int studentId) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        Statement groupStatement = connection.createStatement();
        ResultSet groupIdSet = groupStatement.executeQuery("SELECT `Group Id` FROM Student_To_Group WHERE `Student Id`=" + studentId);
        int groupId = 0;
        if (groupIdSet.next()) {
            groupId = groupIdSet.getInt("Group Id");
        }
        groupIdSet.close();
        groupStatement.close();
        return groupId;
    }


    @Override
    public void addStudent(ResultWrapper resultWrapper, Student std) throws SQLException {
        Connection connection = resultWrapper.getConnection();
        String query = "INSERT INTO student(Name, Surname, Age, Faculty, Profession, Course, ExamScore, ifPaidStudy) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";


        try (PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, std.getName());
            preparedStatement.setString(2, std.getSurname());
            preparedStatement.setInt(3, std.getAge());
            preparedStatement.setString(4, std.getFaculty());
            preparedStatement.setString(5, std.getProfession());
            preparedStatement.setInt(6, std.getCourse());
            preparedStatement.setDouble(7, std.getExamScore());
            preparedStatement.setBoolean(8, std.isIfPaidStudy());

            int rowsAffected = preparedStatement.executeUpdate();

            if (rowsAffected > 0) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int lastInsertedId = generatedKeys.getInt(1);
                    String query2 = "INSERT INTO Student_To_Group(`Student Id`,`Group Id`) VALUES(?,?)";
                    try (PreparedStatement preparedStatement2 = connection.prepareStatement(query2)) {
                        preparedStatement2.setInt(1, lastInsertedId);
                        preparedStatement2.setInt(2, std.getGroupId());

                        int rowsAffected2 = preparedStatement2.executeUpdate();
                        if (rowsAffected2 > 0) {
                            System.out.println("Student inserted successfully.");
                        }
                    } catch (SQLException e) {
                        System.out.println("Student added but failed to connect with group ID.");
                        e.printStackTrace();
                    }
                }
            } else {
                System.out.println("Student insertion failed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
        resultWrapper.getAll().close();
    }
}
