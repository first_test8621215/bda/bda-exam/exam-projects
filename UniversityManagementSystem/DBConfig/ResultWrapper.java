package UniversityManagementSystem.DBConfig;

import java.sql.Connection;
import java.sql.Statement;

public class ResultWrapper {
    private final Connection connection;
    private final Statement all;

    public ResultWrapper(Connection connection, Statement all) {
        this.connection = connection;
        this.all = all;
    }

    public Connection getConnection() {
        return connection;
    }

    public Statement getAll() {
        return all;
    }
}
