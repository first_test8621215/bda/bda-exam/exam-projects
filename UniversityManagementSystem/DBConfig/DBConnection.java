package UniversityManagementSystem.DBConfig;
import java.sql.*;
import io.github.cdimascio.dotenv.Dotenv;

public class DBConnection {
    public static ResultWrapper openConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        Dotenv dotenv = Dotenv.configure().load();

        String url = dotenv.get("url");
        String username = dotenv.get("username");
        String password = dotenv.get("password");

        Connection connection;

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Statement all;
        try {
            all = connection.createStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return new ResultWrapper(connection, all);
    }
}