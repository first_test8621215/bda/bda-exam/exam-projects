package UniversityManagementSystem.Model;

import java.sql.SQLException;

public class Student extends Person {
    private int studentId;
    private String faculty;
    private String profession;
    private int course;
    private int examScore;
    private boolean ifPaidStudy;
    private int groupId;

    public int getStudentId() {
        return studentId;
    }
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getExamScore() {
        return examScore;
    }

    public void setExamScore(int examScore) {
        this.examScore = examScore;
    }

    public boolean isIfPaidStudy() {
        return ifPaidStudy;
    }

    public void setIfPaidStudy(boolean ifPaidStudy) {
        this.ifPaidStudy = ifPaidStudy;
    }

    public Student(String name, String surname, int age, String faculty, String profession, int course, int examScore, boolean ifPaidStudy, int groupId) throws SQLException {
        super(name, surname, age);
        this.faculty = faculty;
        this.profession = profession;
        this.course = course;
        this.examScore = examScore;
        this.ifPaidStudy = ifPaidStudy;
        this.groupId = groupId;
    }

    public Student(int studentId, String name, String surname, int age, String faculty, String profession, int course, int examScore, boolean ifPaidStudy, int groupId) throws SQLException {
        super(name, surname, age);
        this.studentId = studentId;
        this.faculty = faculty;
        this.profession = profession;
        this.course = course;
        this.examScore = examScore;
        this.ifPaidStudy = ifPaidStudy;
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Id=" + this.getStudentId() +
                ", name='" + this.getName() + '\'' +
                ", surname='" + this.getSurname() + '\'' +
                ", age=" + this.getAge() +
                ", faculty='" + this.getFaculty() + '\'' +
                ", profession='" + this.getProfession() + '\'' +
                ", course=" + this.getCourse() +
                ", examScore=" + this.getExamScore() +
                ", ifPaidStudy=" + this.isIfPaidStudy() +
                ", groupId=" + this.getGroupId() +
                '}';
    }
}
