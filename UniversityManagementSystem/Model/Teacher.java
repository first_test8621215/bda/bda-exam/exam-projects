package UniversityManagementSystem.Model;
import java.sql.SQLException;
import java.util.*;

public class Teacher extends Person {
    private int teacherId;
    private String academicDegree;
    private int salary;
    private ArrayList<Integer> subjectIds;
    public Teacher(String name, String surname, int age, String academicDegree, int salary) throws SQLException {
        super(name, surname, age);
        this.academicDegree = academicDegree;
        this.salary = salary;
    }

    public Teacher(int teacherId, String name, String surname, int age, String academicDegree, int salary) throws SQLException {
        super(name, surname, age);
        this.teacherId = teacherId;
        this.academicDegree = academicDegree;
        this.salary = salary;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public ArrayList<Integer> getSubjectIds() {
        return subjectIds;
    }

    public void setSubject(int subjectId) {
        this.subjectIds.add(subjectId);
    }

    public void deleteSubject(int subjectId) {
        this.subjectIds.remove(subjectId);
    }
    @Override
    public String toString() {
        return "Teacher{" +
                "Id=" + this.getTeacherId() +
                ", name='" + this.getName() + '\'' +
                ", surname='" + this.getSurname() + '\'' +
                ", age=" + this.getAge() +
                ", academicDegree='" + this.getAcademicDegree() + '\'' +
                ", salary=" + this.getSalary() +
                '}';
    }
}
