package UniversityManagementSystem.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Journal {
    private int journalId;
    private int studentId;
    private int teacherId;
    private int groupId;
    private int subjectId;
    private int mark;
    private Date date;

    public int getJournalId() {
        return journalId;
    }

    public void setJournalId(int journalId) {
        this.journalId = journalId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Journal(int journalId, int studentId, int teacherId, int groupId, int subjectId, int mark, String dateString) {
        this.journalId = journalId;
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.groupId = groupId;
        this.subjectId = subjectId;
        this.mark = mark;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Journal(int journalId, int studentId, int teacherId, int groupId, int subjectId, int mark, Date date) {
        this.journalId = journalId;
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.groupId = groupId;
        this.subjectId = subjectId;
        this.mark = mark;
        this.date = date;
    }

    public Journal(int studentId, int teacherId, int groupId, int subjectId, int mark, String dateString) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.groupId = groupId;
        this.subjectId = subjectId;
        this.mark = mark;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Journal(int studentId, int teacherId, int groupId, int subjectId, int mark, Date dateString) {
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.groupId = groupId;
        this.subjectId = subjectId;
        this.mark = mark;
        this.date = dateString;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "journalId=" + journalId +
                ", studentId=" + studentId +
                ", teacherId=" + teacherId +
                ", groupId=" + groupId +
                ", subjectId=" + subjectId +
                ", mark=" + mark +
                ", date=" + date +
                '}';
    }
}