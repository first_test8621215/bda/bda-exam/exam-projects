package UniversityManagementSystem;
import UniversityManagementSystem.Model.Admin;
import UniversityManagementSystem.Model.Student;
import UniversityManagementSystem.Model.Teacher;
import UniversityManagementSystem.Service.UserServiceImpl;

import java.sql.SQLException;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws SQLException {
        UserServiceImpl userService = new UserServiceImpl();

        while (true) {
            Object loggedUser = null;
            while (loggedUser == null) {
                Scanner loginCredentialsScan = new Scanner(System.in);
                System.out.println("Please enter your credentials to access UMS System");
                System.out.print("username: ");
                String username = loginCredentialsScan.nextLine();
                System.out.print("password: ");
                String password = loginCredentialsScan.nextLine();
                loggedUser = userService.logIn(username, password);
            }

            boolean shouldExit;
            while (loggedUser != null) {
                if(loggedUser instanceof Teacher) {
                    Teacher teacher = (Teacher) loggedUser;
                    shouldExit = userService.teacherInterface(teacher);

                    if (shouldExit) {
                        loggedUser = null;
                    }
                }
                else if (loggedUser instanceof Student){
                    Student student = (Student) loggedUser;
                    shouldExit = userService.studentInterface(student);

                    if(shouldExit){
                        loggedUser = null;
                    }
                }
                else if (loggedUser instanceof Admin){
                    Admin admin = (Admin) loggedUser;
                    shouldExit = userService.adminInterface(admin);

                    if (shouldExit) {
                        loggedUser = null;
                    }
                }
                else{
                    System.out.println("Unknown user!");
                    loggedUser = null;
                }
            }
        }
    }
}