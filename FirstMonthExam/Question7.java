package FirstMonthExam;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//7. Write a Java program to sort in ascending and descending order by the length
//of the given array of strings
public class Question7 {
    public static void main(String[] args) {
        List<String> myList = Arrays.asList("Green", "White", "Black", "Pink", "Orange", "Blue", "Champagne", "Indigo", "Ivory");
        Collections.sort(myList, Comparator.comparingInt(String::length));
        System.out.println(myList);
        Collections.sort(myList, Comparator.comparingInt(String::length).reversed());
        System.out.println(myList);

    }
}
