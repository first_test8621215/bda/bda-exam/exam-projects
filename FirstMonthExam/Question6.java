package FirstMonthExam;

// Write a Java program to remove a specified character from a given string.
public class Question6 {
    public static void main(String[] args) {
        String mystring = "The quick brown fox jumps over the lazy dog.";
        String specifiedStringToRemove = "a";

        mystring = mystring.replace(specifiedStringToRemove,"");
        System.out.println(mystring);
    }
}
