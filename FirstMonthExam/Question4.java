package FirstMonthExam;
// Write a Java program to replace a specified character with another character.
public class Question4 {
    public static void main(String[] args) {
        String mystring = "The quick brown fox jumps over the lazy dog.";
        String replacement = "fog";

        mystring = mystring.replace("dog",replacement);
        System.out.println(mystring);
    }
}
