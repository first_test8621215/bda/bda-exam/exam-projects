package FirstMonthExam;

import com.sun.security.jgss.GSSUtil;

import java.util.HashMap;

//5.Write a Java program to count and print all duplicates in the input string.
public class Queston5 {
    public static void main(String[] args) {
        HashMap<String,Integer> comparisonHashMap = new HashMap<>();
        String mystring = "w3resource";

        for (int i=0; i<mystring.length(); i++){
            if (comparisonHashMap.containsKey(String.valueOf(mystring.charAt(i)))){
                comparisonHashMap.put(String.valueOf(mystring.charAt(i)),comparisonHashMap.get(String.valueOf(mystring.charAt(i)))+1);
            }else{
                comparisonHashMap.put(String.valueOf(mystring.charAt(i)),1);
            }
        }
        System.out.println(comparisonHashMap);
        for(String key : comparisonHashMap.keySet()){
            if (comparisonHashMap.get(key)>1){
                System.out.println(key+" value repeated " + comparisonHashMap.get(key) + " times!");
            }
        }
    }
}
