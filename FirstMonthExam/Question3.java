package FirstMonthExam;
//3. Write a Java program to get the character (Unicode code point) before the
//specified index within the string
public class Question3 {
    public static void main(String[] args) {
        String mystring = "abcdefr";

        for(int i=0; i<mystring.length(); i++){
            System.out.println("Unicode Point of " + i + " element of string: " + mystring.codePointAt(i));
        }
    }
}
