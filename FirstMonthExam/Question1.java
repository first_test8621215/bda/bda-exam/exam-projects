package FirstMonthExam;

import java.util.Scanner;

public class Question1 {
    public static void main(String[] args) {
        Scanner valueScan = new Scanner(System.in);
        System.out.print("Value: ");
        String value = valueScan.nextLine();
        StringBuilder reverseCheck = new StringBuilder(value).reverse();
        if (value.equals(String.valueOf(reverseCheck))){
            System.out.println("Palindrome");
        }
    }
}
