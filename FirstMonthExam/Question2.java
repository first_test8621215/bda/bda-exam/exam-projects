package FirstMonthExam;
//1. Sum the lengths of  A and B .
//2. Determine if  A is lexicographically larger than B (i.e.: does B come before A in
//the dictionary?).
//3. Capitalize the first letter in A and  B and print them on a single line, separated
//by a space
public class Question2 {
    public static void main(String[] args) {
        String a = "xbcd";
        String b = "ccde";

        System.out.println("First Answer: ");
        System.out.println(a.length()+b.length());

        System.out.println("Second Question:");
        for(int i=0; i<a.length(); i++){
            if ((int) a.charAt(i) == (int) b.charAt(i)){
                continue;
            }else if((int) a.charAt(i) < (int) b.charAt(i)){
                System.out.println("a is bigger");
                break;
            }else{
                System.out.println("b is bigger");
                break;
            }
        }
        System.out.println("Third Question: ");
        System.out.println(a.substring(0,1).toUpperCase());
        System.out.println(b.substring(0,1).toUpperCase());
    }

}