package FirstMonthExam;

import Lesson4.FunctionFoo;

public class Question9 {
    public static void main(String[] args) {
        Sports newSport = new Sports();
        newSport.getNumberOfTeamMembers();

        Soccer newSoccer = new Soccer();
        newSoccer.getNumberOfTeamMembers();
    }
}

class Sports {
    String getName() {
        return "Generic Sports";
    }

    void getNumberOfTeamMembers() {
        System.out.println("Each team has n players in " + getName());
    }
}

class Soccer extends Sports {
    @Override
    String getName() {
        return "Soccer Class";
    }

    @Override
    void getNumberOfTeamMembers(){
        System.out.println("Each team has 11 players in " + getName());
    }
}

