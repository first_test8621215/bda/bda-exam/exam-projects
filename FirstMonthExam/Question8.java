package FirstMonthExam;
//abstract class Book{
//    String title;
//    abstract void setTitle(String s);
//String getTitle(){
//        return title;
//    }
//}
//If you try to create an instance of this class like the following line you will get an error:
//Book new_novel=new Book();
//You have to create another class that extends the abstract class. Then you can create an instance
//of the new class.
//Notice that setTitle method is abstract too and has no body. That means you must implement the
//body of that method in the child class.
//In the editor, we have provided the abstract Book class and a Main class. In the Main class, we
//created an instance of a class called MyBook. Your task is to write just the MyBook class.
//Your class mustn't be public.
public class Question8 {
    public static void main(String[] args) {
        MyBook newBook = new MyBook();
        System.out.println(newBook.setTitle("Alice in the Wonderland"));

    }
}

abstract class Book{
    public abstract String setTitle(String TitleName);
}

class MyBook extends Book{
    String Title;

    @Override
    public String setTitle(String TitleName) {
        Title = TitleName;
        return "Title is " + Title;
    }
}
