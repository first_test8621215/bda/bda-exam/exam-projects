package ContestantApp.MyExceptions;

public class PasswordInvalidException2 extends RuntimeException{
    public PasswordInvalidException2(){
        System.out.println("!! Password can't be shorter than 8 characters and must contains upper and lower cases !!");
    }
}
