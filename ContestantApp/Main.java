package ContestantApp;

import ContestantApp.Model.User;
import ContestantApp.MyExceptions.PasswordInvalidException1;
import ContestantApp.MyExceptions.PasswordInvalidException2;
import ContestantApp.MyExceptions.UserExistsException;
import ContestantApp.Service.ContestantMethodsImpl;
import ContestantApp.Service.UserMethodsImpl;

import java.util.InputMismatchException;
import java.util.Scanner;

import static ContestantApp.Model.Contestant.contestants;

public class Main {
    public static void main(String[] args){
        UserMethodsImpl userMethods = new UserMethodsImpl();
        ContestantMethodsImpl contestantMethods = new ContestantMethodsImpl();
        Scanner isLoginScan = new Scanner(System.in);
        boolean isLoggedIn = false;

        while (true) {
            while (!isLoggedIn) {
                System.out.print("\n" +
                        "1.Login\n" +
                        "2.Register\n" +
                        "Your Option: ");

                int isLogin;
                try {
                    isLogin = isLoginScan.nextInt();
                }catch (InputMismatchException msg){
                    System.out.println("!! Please choose element from list !!!");
                    isLoginScan.nextLine();
                    continue;
                }

                switch (isLogin) {
                    case 1:
                        isLoggedIn = userMethods.checkUser();
                        break;

                    case 2:
                        System.out.println(userMethods.createUser());
                }
            }

            Scanner choiceScan = new Scanner(System.in);

            while (isLoggedIn) {
                {
                    System.out.print("\n1. Yarışmacıları register et\n" +
                            "2. Yarışmacılara Bax\n" +
                            "3. Yarışmaya başla\n" +
                            "4. Logout\n" +
                            "5. Exit" +
                            "\n" +
                            "Your Option: ");
                }

                int choice = choiceScan.nextInt();
                switch (choice) {
                    case 1:
                        Scanner contestantCountScan = new Scanner(System.in);
                        Scanner contestantNameScan = new Scanner(System.in);
                        System.out.print("How many contestants you want to register?: ");
                        int contestantCount = contestantCountScan.nextInt();
                        for (int j = 0; j < contestantCount; j++) {
                            System.out.print("Contestant Name: ");
                            String contestantName = contestantNameScan.nextLine();
                            System.out.println(contestantMethods.registerContestant(contestantName));
                        }
                        break;
                    case 2:
                        contestantMethods.showContestants();
                        break;
                    case 3:
                        System.out.println(contestantMethods.startContest(contestants.size()));
                        break;
                    case 4:
                        isLoggedIn = false;
                        break;
                    case 5:
                        System.exit(0);
                }
            }
        }
    }
}