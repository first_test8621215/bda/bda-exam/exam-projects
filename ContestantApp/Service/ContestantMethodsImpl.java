package ContestantApp.Service;

import ContestantApp.Model.Contestant;

import java.util.Scanner;

import static ContestantApp.Model.Contestant.contestants;

public class ContestantMethodsImpl implements ContestantMethods {
    @Override
    public String registerContestant(String name) {

        for (int i = 1; i <= contestants.size(); i++) {
            if (contestants.get(i).equals(name)) {
                return "!! Contestant exists in this name !!";
            }
        }
        new Contestant(name);
        return "!! Contestant is created successfully !!";
    }

    @Override
    public String startContest(int contestantCount) {
        int randomNumber = (int) (Math.random() * contestantCount) + 1;
        return contestants.get(randomNumber) + " WON !";
    }

    @Override
    public void showContestants() {
        System.out.println(contestants);

    }
}

