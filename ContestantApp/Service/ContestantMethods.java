package ContestantApp.Service;

public interface ContestantMethods {
    String registerContestant(String name);

    public String startContest(int contestantCount);

    public void showContestants();
}
