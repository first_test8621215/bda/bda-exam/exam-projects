package ContestantApp.Service;

import java.util.ArrayList;
import java.util.List;

public interface UserMethods {
    public boolean checkUser();

    public String createUser();
}
