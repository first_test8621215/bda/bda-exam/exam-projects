package ContestantApp.Service;


import ContestantApp.Model.User;
import ContestantApp.MyExceptions.PasswordInvalidException1;
import ContestantApp.MyExceptions.PasswordInvalidException2;
import ContestantApp.MyExceptions.UserExistsException;

import java.util.Scanner;

import static ContestantApp.Model.User.allUsers;

public class UserMethodsImpl implements UserMethods {
    @Override
    public boolean checkUser() {
        int attemptCount = 0;
        while (attemptCount < 3) {
            Scanner credentialScan = new Scanner(System.in);
            System.out.print("Username: ");
            String username = credentialScan.nextLine();
            System.out.print("Password: ");
            String password = credentialScan.nextLine();

            for (int i = 1; i <= allUsers.size(); i++) {
                if (allUsers.get(i).get("username").equals(username) && allUsers.get(i).get("password").equals(password)) {
                    System.out.println("!! Successfully Logged in !!\n");
                    return true;
                }
            }
            System.out.println("!! Unsuccessful login !!\n");
            attemptCount += 1;
        }


        if (attemptCount == 3) {
            System.out.println("!!! You banned !!!");
            System.exit(0);
        }
        return false;
    }

    @Override
    public String createUser() throws UserExistsException, PasswordInvalidException1, PasswordInvalidException2 {
        Scanner registerCredentialScan = new Scanner(System.in);
        System.out.print("Username: ");
        String username = registerCredentialScan.nextLine();
        System.out.print("Password: ");
        String password = registerCredentialScan.nextLine();
        // , eger 8 simvoldan kicikdirse + icerisinde boyuk herf
        //yoxdursa + icerisinde en azi 2 reqem ve digger simvollardan yoxdursa exception
        //atsin
        String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] symbols = {"!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=", "<", ">", ",", ".", "/", "?", "`", "~"};

        int repeatedNumbers = 0;
        int repeatedSymbols = 0;

        try {
            if (password.length() < 8 || password.toLowerCase().equals(password) || password.toUpperCase().equals(password)) {
                throw new PasswordInvalidException2();
            }
        } catch (PasswordInvalidException2 msg) {
            return "";
        }

        for (String symbol : symbols) {
            if (password.contains(symbol)) {
                repeatedSymbols += 1;
            }
        }

        for (String number : numbers) {
            if (password.contains(number)) {
                repeatedNumbers += 1;
            }
        }

        try {
            if (repeatedSymbols == 0 || repeatedNumbers == 0) {
                throw new PasswordInvalidException1();
            }
        } catch (PasswordInvalidException1 msg) {
            return "";
        }

        try {
            for (int i = 1; i <= allUsers.size(); i++) {
                if (allUsers.get(i).get("username").equals(username)) {
                    throw new UserExistsException();
                }
            }
        } catch (UserExistsException msg) {
            return "";
        }

        new User(username, password);
        return "! User successfully created !\n";
    }
}
