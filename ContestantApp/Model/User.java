package ContestantApp.Model;

import java.util.HashMap;

public class User {
    private static int IDCounter = 1;
    private int ID;
    public static HashMap<Integer, HashMap<String, String>> allUsers = new HashMap<>();
    HashMap<String, String> userDetails = new HashMap<>();
    String username;
    String password;

    static {
        new User("Ali", "12345");
    }

    public User(String username, String password) {
        this.ID = IDCounter++;
        this.username = username;
        this.password = password;

        userDetails.put("username", username);
        userDetails.put("password", password);

        allUsers.put(this.ID, userDetails);
    }
}
