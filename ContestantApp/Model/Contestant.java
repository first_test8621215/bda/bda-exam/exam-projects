package ContestantApp.Model;

import java.util.HashMap;

public class Contestant {
    private static int IDCounter = 1;
    private int ID;
    public static HashMap<Integer, String> contestants = new HashMap<>();
    String contestantName;

    public Contestant(String contestantName) {
        this.ID = IDCounter++;
        this.contestantName = contestantName;

        contestants.put(this.ID,this.contestantName);
    }
}
